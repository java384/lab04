import paket1.A;

public class C extends A {
    public C(String name) {
        super(name);
    }

    @Override
    public String getName() {
        return "C: " + name;
    }

    public String examine(C c) {
        return "C vidi: " + c.name;
    }


}
