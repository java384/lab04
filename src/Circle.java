public class Circle extends Shape {



    public static final double PI = 3.14159;
    protected double r;

    public Circle(double r) {
        checkRadius(r);
        this.r = r;
    }

    public double getRadius() { return r; }

    public void setRadius(double r) {
        checkRadius(r);
        this.r = r;
    }


    void checkRadius(double radius){
        if (radius < 0.0)
            throw new
                    IllegalArgumentException ("Radius ne smije biti negativan" );
    }


    public double area() {
        return PI * r * r;

    }


    public double circumference() {
        return 2 * PI * r;
    }


}




