public class main {
    public static void main(String[] args) {

        Circle c1 = new Circle(34);

        c1.setRadius(32);
        System.out.println(c1.getRadius());

        System.out.println(c1.area());
        System.out.println(c1.circumference());

        Shape[] shapes = new Shape[3];

        shapes[0] = new Circle(2.0);
        shapes[1] = new Rectangle(1.0, 3.0);
        shapes[2] = new Rectangle(4.0, 2.0);

        double povrsina = 0;
        for(int i = 0; i < shapes.length; i++)
            povrsina += shapes[i].area();

        System.out.println("Povrsina:");
        System.out.println(povrsina);

    }

}