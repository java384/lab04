package paket1;

public class B extends A {

    public B(String name) {
        super(name);
    }

    @Override
    public String getName() {
        return "B: " + name;
    }

    public String examine(A a) {
        return "B vidi: " + a.name;
    }

}

